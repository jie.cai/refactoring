package com.twu.refactoring;

import java.util.List;

public class Order {
    private String customerName;
    private String customerAddress;
    private List<LineItem> lineItems;

    public Order(String customerName, String customerAddress, List<LineItem> lineItems) {
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.lineItems = lineItems;
    }

    String getCustomerName() {
        return customerName;
    }

    String getCustomerAddress() {
        return customerAddress;
    }

    List<LineItem> getLineItems() {
        return lineItems;
    }

    Double getTotalAmount() {
        double totalAmount = 0d;
        for (LineItem lineItem : this.getLineItems()) {
            totalAmount += lineItem.totalAmount() + lineItem.totalSalesTax();
        }
        return totalAmount;
    }

    Double getTotalSalesTax() {
        double totalSalesTax = 0d;
        for (LineItem lineItem : lineItems) {
            totalSalesTax += lineItem.totalSalesTax();
        }
        return totalSalesTax;
    }
}
